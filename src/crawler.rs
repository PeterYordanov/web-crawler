use reqwest::Url;
use select::document::Document;
use select::predicate::Name;
use select::predicate::Predicate;
use std::collections::HashSet;
use std::path::Path;
use tokio::runtime::Runtime;

use crate::result::Result;

pub fn get_links_from_html(html: &str) -> HashSet<String> {
    Document::from(html)
        .find(Name("a").or(Name("link")))
        .filter_map(|n| n.attr("href"))
        .filter(has_extension)
        .filter_map(normalize_url)
        .collect::<HashSet<String>>()
}

pub fn normalize_url(url: &str) -> Option<String> {
    let new_url = Url::parse(url);
    match new_url {
        Ok(new_url) => {
            if let Some("discord.com") = new_url.host_str() {
                Some(url.to_string())
            } else {
                None
            }
        }
        Err(_e) => {
            if url.starts_with('/') {
                Some(format!("https://discord.com{}", url))
            } else {
                None
            }
        }
    }
}

pub fn fetch_url(url: &str) -> Result<String> {
    let runtime = Runtime::new().unwrap();

    let res = runtime.block_on(reqwest::get(url)).unwrap();

    //println!("Status: {}", res.status());

    let body = res.text();
    Ok(runtime.block_on(body).unwrap())
}

pub fn has_extension(url: &&str) -> bool {
    Path::new(&url).extension().is_none()
}
