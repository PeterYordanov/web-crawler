use rayon::prelude::*;
use std::collections::HashSet;
use std::time::Instant;

mod errors;
mod result;
mod file_writer;
mod crawler;
use crate::result::Result;
use crate::errors::Error;
use crate::file_writer::write_file;
use crate::crawler::get_links_from_html;
use crate::crawler::fetch_url;

fn main() -> Result<()> {
    let now = Instant::now();

    let origin_url = "https://discord.com/";

    let body = fetch_url(origin_url).unwrap();

    write_file("", &body)?;
    let mut visited = HashSet::new();
    visited.insert(origin_url.to_string());
    let found_urls = get_links_from_html(&body);
    let mut new_urls = found_urls
        .difference(&visited)
        .map(|x| x.to_string())
        .collect::<HashSet<String>>();

    while !new_urls.is_empty() {
        let (found_urls, errors): (Vec<Result<HashSet<String>>>, Vec<_>) = new_urls
            .par_iter()
            .map(|url| -> Result<HashSet<String>> {
                let body = fetch_url(url).unwrap();
                write_file(&url[origin_url.len() - 1..], &body)?;

                let links = get_links_from_html(&body);
                println!("Visited: {} found {} links", url, links.len());
                Ok(links)
            })
            .partition(Result::is_ok);

        visited.extend(new_urls);
        new_urls = found_urls
            .into_par_iter()
            .map(Result::unwrap)
            .reduce(HashSet::new, |mut acc, x| {
                acc.extend(x);
                acc
            })
            .difference(&visited)
            .map(|x| x.to_string())
            .collect::<HashSet<String>>();
        println!("New urls: {}", new_urls.len());
        println!(
            "Errors: {:#?}",
            errors
                .into_iter()
                .map(Result::unwrap_err)
                .collect::<Vec<Error>>()
        )
    }
    
    println!("Elapsed time: {}", now.elapsed().as_secs());
    Ok(())
}