use std::io::Error as InputOutputError;

#[derive(Debug)]
pub enum Error {
    Write { url: String, e: InputOutputError },
    Fetch { url: String, e: reqwest::Error },
}

impl<S: AsRef<str>> From<(S, InputOutputError)> for Error {
    fn from((url, e): (S, InputOutputError)) -> Self {
        Error::Write {
            url: url.as_ref().to_string(),
            e,
        }
    }
}

impl<S: AsRef<str>> From<(S, reqwest::Error)> for Error {
    fn from((url, e): (S, reqwest::Error)) -> Self {
        Error::Fetch {
            url: url.as_ref().to_string(),
            e,
        }
    }
}